# VARIABLES
SHELL = /bin/sh
NAME = kitty-theme-selector

PREFIX ?= /usr
SHARE != [ -d ${PREFIX}/share/man ] && echo /share || true

install:
	install -Dm 775 ktheme -t $(DESTDIR)$(PREFIX)/bin/

clean:
	$(shell [ -f $HOME/.config/kitty-theme-selector/src ] && rm $HOME/.config/kitty-theme-selector/src )
	@echo "done"

.PHONY: build install


